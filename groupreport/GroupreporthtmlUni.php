<!DOCTYPE html>
<html lang="de">
<head>
    <title>Lerngruppenmitgliederzahlen Studienstart</title>
    <meta charset="UTF-8">
    <style>
        body {
            font-family: Lato, "Open Sans", Arial, Helvetica, sans-serif ;
        }

        .learngp {
            border: 2px solid #ddd;
            border-collapse: collapse;
            padding: 8px;
        }

        .learngp th {
            padding-top: 12px;
            padding-bottom: 12px;
            text-align: center;
            background-color: #e2001a;
            color: white;
            border: 2px solid #ddd;
        }

        .learngp td {
            padding: 6px;
            border: 1px solid #ddd;
        }

        .learngp tr.subtotal {
            background-color: #f2fff2;
        }

        .learngp tr.sum_faculties {
            background-color: #e0ffe0;
        }

        .learngp tr.sum_total {
            background-color: #c0ffea;
        }

        .learngp tr.divider td {
            height: 2px;
            font-size: 0;
            line-height: 0;
            padding: 0;
            margin: 0;
            background-color: #aaaaaa;
        }

        .right {
            text-align: right
        }

        .light_gray {
            background-color: #f2f2f2;
        }

        .light_green {
            background-color: #f2fff2;
        }

        .light_blue {
            background-color: #ebebff;
        }
    </style>

</head>
<body>

<?php

$sitetodo = "ALL";

if (count($argv) > 1) $sitetodo = strtoupper($argv[1]);

echo "<h3><strong>Lerngruppenmitgliederzahlen:</strong></h3><p>";

setlocale(LC_TIME, "de_DE.utf8");

date_default_timezone_set("Europe/Berlin");

echo strftime("%A, %d %B %Y, %H:%M", time());

echo "</p><p>Version: 20220501-1</p>"; // todo --> Dhouha fragen wg. aktueller Versionsnummer

error_reporting(E_ERROR);
require_once('/var/ilias-gpsync/groupreport/ini/studienstart.ini.php');
require_once('/var/ilias-gpsync/lib/iliasfunctions2.php');
require_once('/var/ilias-gpsync/lib/translator.php');

echo "<p>System: $ILIAS_url<p/>";

if (empty($credential)) die ('platform ini missing');

$Jahr = '2022';


//construct array $orgunits[name][ref,pos,affiliation] by function call childOus for fixed toplevel- Orgunit
//echo "hier die baseOuId: $baseOuId <br>";
$dummy = childOus($baseOuId);
//echo "dummy ist: $dummy <br>";
$first = true;
$acadfirst = true;
$rowcount = 0;
$menteesumAcad = 0;
$menteesumAll = 0;
$menteesumSty = 0;
$study = "";
//echo "hallo $orgunits\r\n";

foreach ($orgunits as $srcOu => $srcOuData) {

//	echo "Gruppen aus der Studienakademie $srcOu\r\n";

    $site = $siteshort[$srcOuData['affiliation']];
    if (($site == $sitetodo) or ($sitetodo == "ALL")) {
        if ($orgunits[$srcOu]['pos'] == "site") {
            $tgtSite = $srcOuData['affiliation'];
            if (!$first) {
                echo "<tr class='subtotal'><td><i>Zwischensumme $study</i></td><td class='right'>--</td><td class='right'>$menteesumSty</td></tr>";
                $study = $srcOu;
                $menteesumSty = 0;
                echo "<tr class='sum_total'><td>Summe $orgunit</td><td class='right'>--</td><td class='right'>$menteesumAcad</td></tr>";
                echo "<p/> <p></p></table class='learngp'>";
                $menteesumAcad = 0;
                $rowcount = 0;
                $acadfirst = true;
            }

            echo "<table class='learngp'>";
            echo "<tr>";
            echo "<th colspan=3>";
            echo "Gruppen aus der Studienakademie $tgtSite\r\n";
            echo "</th>";
            echo "</tr>";
            $first = false;
        }

        // Container- OUs sind hier ausgenommen, da sie keine Mentees enthalten sollen
        // Jetzt Gruppen suchen und auszählen

        if (($orgunits[$srcOu]['pos'] == "leaf") and (in_array(substr($srcOu, 0, 4), $Faculties))) {

            $srcFields = explode(' ', $srcOu, 3);
            $dateFields = explode('.', $srcFields[2], 2);

            $tgtLerngruppe = 'Lerngruppe Pre ' . $site . ' ' . $srcFields[1] . ' ' . $dateFields[0] . '.' . substr($dateFields[1], 0, 5);

            $iliasGruppe = $ILIAS_Soap->__soapCall("groupExists", array($session, $tgtLerngruppe));

            if ($iliasGruppe != 0) {

                //Lerngruppe suchen
                $tgtRoleLerngruppe = getRolesForName($tgtLerngruppe)['member'];
                $groupmembercount = count(getRoleHolders($tgtRoleLerngruppe));

                if (($rowcount % 2) == 1) {
                    echo "<tr class='light_gray'>";
                } else {
                    echo "<tr>";
                }

            } else {
                if (($rowcount % 2) == 1) {
                    echo "<tr class='light_blue'>";
                } else {

                    echo "<tr>";
                }
            }
            echo "<td>";
            echo $tgtLerngruppe;
            echo "</td>";

            echo "<td class='right'>";
            if ($iliasGruppe != 0) {
                echo "$groupmembercount";
                $menteesumAcad += $groupmembercount;
                $menteesumAll += $groupmembercount;
                $menteesumSty += $groupmembercount;

            } else {
                echo "-";
            }
            echo "</td>";
            echo "</tr>";

            // Die einzelnen Lerngruppen, bzw. Studiengänge fakultätsweise aufaddieren / Stefko, 28.03.2023
            switch ($srcFields[0]) {
                case "GES":
                    $facultiesCountGES += $groupmembercount;
                    break;
                case "TEC":
                    $facultiesCountTEC += $groupmembercount;
                    break;
                case "WIR":
                    $facultiesCountWIR += $groupmembercount;
                    break;
                case "WEI":
                    $facultiesCountWEI += $groupmembercount;
                    break;
                case "SOZ":
                    $facultiesCountSOZ += $groupmembercount;
                    break;
            }

            $rowcount++;
        }
    }

    if (($orgunits[$srcOu]['pos'] == "container") and (($site == $sitetodo) or ($sitetodo == 'ALL'))) {
        if ($study != $srcOu) {
            if (!$acadfirst) {
                echo "<tr class='light_green'><td><i>Zwischensumme $study</i></td><td class='right'>--</td><td class='right'>$menteesumSty</td></tr>\r\n";
                $study = $srcOu;
                $menteesumSty = 0;
            } else {
                $acadfirst = false;
                $study = $srcOu;
            }
        }
    }
}
// end foreach loop

echo "<tr class='light_green'><td><i>Zwischensumme $study</i></td><td class='right'>--</td><td class='right'>$menteesumSty</td></tr>";

echo "<tr class='divider'><td colspan=3></td></tr>";

if ($facultiesCountGES > 0) {echo "<tr class='sum_faculties'><td colspan='2'>Summe Fakultät GES:</td><td class='right'><strong>$facultiesCountGES</strong></td></tr>";}
if ($facultiesCountTEC > 0) {echo "<tr class='sum_faculties'><td colspan='2'>Summe Fakultät TEC:</td><td class='right'><strong>$facultiesCountTEC</strong></td></tr>";}
if ($facultiesCountWIR > 0) {echo "<tr class='sum_faculties'><td colspan='2'>Summe Fakultät WIR:</td><td class='right'><strong>$facultiesCountWIR</strong></td></tr>";}
if ($facultiesCountWEI > 0) {echo "<tr class='sum_faculties'><td colspan='2'>Summe Fakultät WEI:</td><td class='right'><strong>$facultiesCountWEI</strong></td></tr>";}
if ($facultiesCountSOZ > 0) {echo "<tr class='sum_faculties'><td colspan='2'>Summe Fakultät SOZ:</td><td class='right'><strong>$facultiesCountSOZ</strong></td></tr>";}

echo "<tr class='sum_total'><td><strong>Gesamtsumme: $orgunit</strong></td><td class='right'>--</td><td class='right'><strong>$menteesumAcad</strong></td></tr>";
echo "</table>";
if ($sitetodo == "ALL") echo "<p/><p/>Mentees alle Studienakademien: $menteesumAll<p/>";

// finally close session
$ILIAS_Soap->logout($session);

echo "</p>";

?>
</body>
</html>
