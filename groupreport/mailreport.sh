if [[ -f /var/ilias-gpsync/groupreport/output/reportsmailed.txt ]]; then
  rm /var/ilias-gpsync/groupreport/output/reportsmailed.txt
fi

if [[ ! -f /var/ilias-gpsync/groupreport/output/reportsdone.txt ]]; then
	echo give me 5 minutes
	sleep 300
fi 
if [[ -f /var/ilias-gpsync/groupreport/output/reportsdone.txt ]]; then
	/usr/bin/php /var/ilias-gpsync/groupreport/mailreports.php
	mv /var/ilias-gpsync/groupreport/output/reportsdone.txt /var/ilias-gpsync/groupreport/output/reportsmailed.txt

fi
