<?php
$Jahr=2023;

$siteshort['Karlsruhe'] = 'KA';
$siteshort['Heilbronn'] = 'HN';
$siteshort['Heidenheim'] = 'HDH';
$siteshort['Mosbach'] = 'MOS';
$siteshort['Mannheim'] = 'MA';
$siteshort['Ravensburg'] = 'RV';
$siteshort['Lörrach'] = 'LOE';
$siteshort['Villingen-Schwenningen'] = 'VS';
$siteshort['Stuttgart'] = 'S';
$siteshort['DHBW'] = 'DHBW';

$monthname['01'] = 'Januar';
$monthname['02'] = 'Februar';
$monthname['03'] = 'März';
$monthname['04'] = 'April';
$monthname['05'] = 'Mai';
$monthname['06'] = 'Juni';
$monthname['07'] = 'Juli';
$monthname['08'] = 'August';
$monthname['09'] = 'September';
$monthname['10'] = 'Oktober';
$monthname['11'] = 'November';
$monthname['12'] = 'Dezember';
// Einträge für HN
$monthname['20'] = 'A-Zyklus';
$monthname['21'] = 'B-Zyklus';

$studiengang['ET'] = 'Elektrotechnik';
$studiengang['HT'] = 'Holztechnik';
$studiengang['MT'] = 'Mechatronik';
$studiengang['MT-ELM'] = 'Mechatronik Elektromobilität';
$studiengang['MB'] = 'Maschinenbau';
$studiengang['MB-KON'] = 'Maschinenbau Konstruktion';
$studiengang['MB-KST'] = 'Maschinenbau Kunststofftechnik';
$studiengang['MB-LEB'] = 'Maschinenbau Lebensmitteltechnik';
$studiengang['MB-VER'] = 'Maschinenbau Verfahrenstechnik';
$studiengang['MB-VIE'] = 'Maschinenbau Virtual Engineering';
$studiengang['WI'] = 'Wirtschaftsinformatik';
$studiengang['BWL-DLM'] = 'Dienstleistungsmanagement';
$studiengang['BWL-FOM'] = 'Food Management';
$studiengang['BWL-HAN'] = 'Handel';
$studiengang['DBM'] = 'Digital Business Management';
$studiengang['IT'] = 'Informatik';
$studiengang['INF'] = 'Informatik';
$studiengang['MED'] = 'Medien';
$studiengang['RSW'] = 'RSW';
$studiengang['PT'] = 'Papiertechnik';
$studiengang['PY'] = 'Physiotherapie';
$studiengang['GES'] = 'Gesundheitsmanagement';
$studiengang['ST'] = 'Sicherheitswesen';
$studiengang['SIW'] = 'Sicherheitswesen';
$studiengang['Other'] = 'Anderer';
$studiengang['Weitere'] = 'Weitere';
$studiengang['WEI'] = 'Weitere';
$studiengang['WING'] = 'Wirtschaftsingenieurwesen';
$studiengang['WING-IPL'] = 'Wirtschaftsingenieurwesen Internationale Produktion und Logistik';
$studiengang['WING-ITP'] = 'Wirtschaftsingenieurwesen Internationales Technisches Projektmanagement';
$studiengang['WING-ITV'] = 'Wirtschaftsingenieurwesen Internationales Technisches Vertriebsmanagement';
$studiengang['WING-PM'] = 'Wirtschaftsingenieurwesen Innovations- und Produktmanagement';
$studiengang['MATR'] = 'Management Trinational';
$studiengang['BA'] = 'Bank';
$studiengang['IN'] = 'Industrie';
$studiengang['IGE'] = 'Interprofessionelle Gesundheitsversorgung';
$studiengang['VER'] = 'Versicherung';
$studiengang['HOGA'] = 'Hotellerie und Gastronomie';
$studiengang['BWL-GM'] = 'Gesundheitsmanagement';
$studiengang['BWL-IB'] = 'International Business';
$studiengang['BWL-PE'] = 'Personalmanagement';
$studiengang['BWL-FD'] = 'Finanzdienstleistungen';
$studiengang['BWL-TO'] = 'Tourismus, Hotellerie und Gastronomie';
$studiengang['BWL-SPE'] = 'Spedition, Transport & Logistik';
$studiengang['BAU-FAS'] = 'Fassadentechnik';
$studiengang['BAU-OEF'] = 'Öffentliches Bauen';
$studiengang['BAU-PRO'] = 'Bauingenieurwesen Projektmanagement';
$studiengang['TL'] = 'Transport und Logistik';
$studiengang['MAT'] = 'Mathematik';
//
//loe und HDH
$studiengang['WIAM'] = 'Wirtschaftsinformatik - Application Management';
$studiengang['WIDS'] = 'Wirtschaftsinformatik - Data Science';
$studiengang['WIR-BWL'] = 'Betriebswirtschaft';
$studiengang['SOZ-SOZ'] = 'Sozialwesen';
$studiengang['GES-GES'] = 'Gesundheit';
$studiengang['BWL-IBM'] = 'International Business Management Trinational';
// Karlsruhe
$studiengang['GES-GP'] = 'Pflegewissenschaften';
$studiengang['GES-HW'] = 'HebammenWS';
$studiengang['GES-AA'] = 'Arztassistenz';
$studiengang['BWL-DFM'] = 'DFM';
$studiengang['BWL-SP'] = 'Steuern';
$studiengang['BWL-VER'] = 'Versicherung';
$studiengang['BWL-VER'] = 'Bank';
$studiengang['BWL-IN'] = 'Industrie';
$studiengang['BWL-DCM'] = 'DCM';
$studiengang['BWL-DBM'] = 'DBM';
$studiengang['WIS'] = 'Unternehmertum';
$studiengang['UN'] = 'Unternehmertum';
//
// MOSBACH

// mannheim
//
$studiengang['WIR'] = 'WIRTSCHAFT';
$studiengang['BWL-DM'] = 'DM';
$studiengang['BWL-RSW'] = 'RSW';
$studiengang['BWL-WI'] = 'WI';
$studiengang['CT'] = 'CT';
$studiengang['GP'] = 'GP';
$studiengang['IE'] = 'IE';
$studiengang['ALL'] = 'Alle';
$studiengang['ALLG'] = 'Allgemein';
$studiengang['GESU'] = 'Gesundheit und Soziales';

$Faculties[] = "TEC ";
$Faculties[] = "WIR ";
$Faculties[] = "WEI ";
//new
$Faculties[] = "GES ";
$Faculties[] = "SOZ ";
