<?php
//Version without nusoap.php. requires php-soap available, runnabel on php7. 
//20190527
// ***** Correctness to be checked!

//client_id is reqired if pluginslot for webservices should be available
$wsdlpage = $ILIAS_url . "/webservice/soap/server.php?wsdl&client_id=" . $credential['client']; 
//$wsdlpage = $ILIAS_url . "/webservice/soap/server.php?wsdl"; 
$ILIAS_Soap = new SoapClient($wsdlpage,array('encoding'=>'utf8'));
$session = $ILIAS_Soap->login($credential['client'],$credential['username'],$credential['password']);



function addGroup($groupname, $target = "default") 
{
// todo: find appropriate target container ref id, hardcoded for now
// distinguish by ESC

global $ILIAS_Soap;
global $session;

// $target should be set, "default" is deprecated

/*
if ($target == "default") {
	echo "***** deprecated, update call to addGroup *****\r\n";

	//awz

	$targetcontainer = 183;

	//root->Material ESCs -> xx eMentoring Gruppen yyyy

	// eMentoring Gruppen HN
	if (substr($groupname,0,2) == "HN") $targetcontainer = 1947;
	//eMentoring Guppen HDH
	if (substr($groupname,0,3) == "HDH") $targetcontainer = 1947;
	//eMentoring Gruppen KA
	if (substr($groupname,0,2) == "KA") $targetcontainer = 1947;

	//Category root-> "Material ESCs"
	if (substr($groupname,0,12) == "ESC Material") $targetcontainer = 184;
	} else {
	$targetcontainer = $target;
	}
*/

$gruppeXML = '<?xml version="1.0" encoding="utf-8"?>';
$gruppeXML.= '<!DOCTYPE group PUBLIC "-//ILIAS//DTD Group//EN" "https://studienstart.dhbw.de/xml/ilias_group_3_10.dtd">';
// known bug: dhbw branch 5.3.12 creates open groups
$gruppeXML.= '<group type="closed">';
$gruppeXML.= "<title>$groupname</title>";
$gruppeXML.= '<owner id="il_0_usr_8801"/>';
$gruppeXML.= "<information/>";
$gruppeXML.= '<registration type="disabled" waitingList="No">';
$gruppeXML.= '<maxMembers enabled="No">0</maxMembers>';
$gruppeXML.= "</registration>";
$gruppeXML.= '<admin id="il_0_usr_8801" notification="Yes"/>';
$gruppeXML.= '<Sort direction="ASC" type="Title"/>';
$gruppeXML.= "</group>";

$iliasGruppe = utf8_encode($ILIAS_Soap->__soapCall("addGroup", array($session, $targetcontainer, $gruppeXML)));

}

function getPositionHolders ($positions)

{
	global $session;
	global $ILIAS_Soap;
	global $dbacc;
// TODO: convert to new webservice via plugin
// make sure parameter is an array to support old style

//echo "passed \r\n";
//print_r ($roles);

if (is_array ($positions)) {
      $postodo = $positions;
      } else{
      $postodo[] = $positions;
      }

//echo "roles to check :\r\n";
//print_r ($rolestodo);

// process all entries to one userlist to aggregate multiple roles

$allnumresults = 0;

// Create connection
$con = mysqli_connect($dbacc['dbservername'], $dbacc['dbusername'], $dbacc['dbpass'], $dbacc['dbname']);

//print_r($postodo);
//array($results);
//$results[] = 'marker';

foreach ($postodo as $pos_id)
    {
  /*
  	$RoleHolders = utf8_encode($ILIAS_Soap->call("getUsersForRole", array($session,$role_id,0,-1)));

	$result_decoded = new SimpleXmlElement( $RoleHolders );

  */
        $pos=1;
        if (substr($pos_id,0,1)=="s") $pos=2;
        $orgutodo=substr($pos_id,1);
	$query = "select user_id from il_orgu_ua where orgu_id=" . $orgutodo . " and position_id=" . $pos;
//	echo $query;
	$sqlresult = mysqli_query($con,$query);
 	$numresults = 0;
	$userlist=array();
 
        while ($result = mysqli_fetch_assoc($sqlresult))
            { 
            $numresults++;
            $uid = $result['user_id'];
            if (!in_array($uid,$userlist)) $userlist[] = $uid;
            }
       
        $allnumresults= $allnumresults + $numresults;
/*
	for ($currentuser=0;$currentuser<$numresults;$currentuser++){

	    $nextresult = $result_decoded->User[$currentuser]->attributes();
	    foreach ($nextresult as $key => $value) {
	       if ($key == 'Id') {
	         $user_id = explode("usr_",$value)[1];
                 // avoid duplicate user entries if same user holdsseveral roles 
                 if (!in_array($user_id,$userlist)) $userlist[] = $user_id;
	         }
	    }
	} 
*/
     }
//$results[] = 'marker';
 
        mysqli_close($con);

	if ($allnumresults > 0) {
	return ($userlist);
	} else {
	return ;
	}
}

function getRoleHolders ($roles)
// extended for positions....
{
	global $session;
	global $ILIAS_Soap;

// make sure parameter is an array to support old style

//echo "passed \r\n";
//print_r ($roles);


if (is_array ($roles)) {
      $rolestodo = $roles;
      } else{
      $rolestodo[] = $roles;
      }

//echo "roles to check :\r\n";
//print_r ($rolestodo);

// process all entries to one userlist to aggregate multiple roles

$allnumresults = 0;
$userlist=array();

foreach ($rolestodo as $role_id)
    {
//	echo "process $role_id\r\n";
	if (!is_numeric(strtolower(substr($role_id,0)))) 	{
		// we have a positioni
		$posusers=array();
		$posusers=getPositionHolders($role_id);
//		print_r($posusers);
		//$userlist=array_merge($userlist,$posusers);
		//dedup-version
		// foreach posusers as currentuser
		// if (!in_array)
		if (count($posusers)>0) {
		  foreach ($posusers as $user_id) {
			if (!in_array($user_id,$userlist)) $userlist[]=$user_id;
			}
	          }
		} else {
    		//we have a true role
		$RoleHolders = $ILIAS_Soap->__soapCall("getUsersForRole", array($session,$role_id,0,-1));

		$result_decoded = new SimpleXmlElement( $RoleHolders );
		$numresults = count($result_decoded->User);
//        	$allnumresults= $allnumresults + $numresults;

		for ($currentuser=0;$currentuser<$numresults;$currentuser++){

	    		$nextresult = $result_decoded->User[$currentuser]->attributes();
	    		foreach ($nextresult as $key => $value) {
	       		if ($key == 'Id') {
	         		$user_id = explode("usr_",$value)[1];
                 		// avoid duplicate user entries if same user holdsseveral roles 
                 		if (!in_array($user_id,$userlist)) $userlist[] = $user_id;
	         		}
	    		}
			}	 
     		}	
	}

 
//	if ($allnumresults > 0) {
	if (count($userlist) > 0) {
	return ($userlist);
	} else {
	return ;
	}
}

function mirrorRole ($sourcerole,$targetrole,$mode="add")

{

	// just in case	
	if (is_null($sourcerole)) return;
	if (is_null($targetrole)) return;
	
	$sourceusers=getRoleHolders($sourcerole);
	$targetusers=getRoleHolders($targetrole);

	global $session;
	global $ILIAS_Soap;

	// mode = add means no users are deleted from Target Role, all other parametes mean users are deleted if not in source role
	
	if ($mode != 'add')  {

		// if source has users check whom to keep/ delete

		if (!empty ($sourceusers) ) {
			// todo: catch empty target
			foreach ($targetusers as $t_user) {
			      if (in_array ($t_user, $sourceusers)) {
	        	 	echo "pass1 to keep: ". $t_user ."\r\n";
	         		} else {
	         		echo "pass1 to drop: ". $t_user ."\r\n";
		 		$result = $ILIAS_Soap->__soapCall("deleteUserRoleEntry", array($session, $t_user,$targetrole));
		        	}
			}
		} else {

		// source ist empty, so delete all. If target is empty, nothing to do.
		//	if ($targetusers != 'empty') {
			if (!empty( $targetusers )) {
				foreach ($targetusers as $t_user) {
	         		echo "pass1e to drop: ". $t_user ."\r\n";
		 		$result = $ILIAS_Soap->__soapCall("deleteUserRoleEntry", array($session, $t_user,$targetrole));
				}	
			}
		}
	}


	// check and add new Users to Target Role (only if source isn't empty) 
 
	//if ($sourceusers != 'empty') {
	if (!empty ($sourceusers)) {
	foreach ($sourceusers as $t_user) {
		// todo: catch empty target = add all from source
     		if (in_array ($t_user, $targetusers)) {
         		echo "pass2 to keep: ". $t_user ."\r\n";
         		} else {
         		echo "pass2 to add: ". $t_user ."\r\n";
	         	$result = $ILIAS_Soap->__soapCall("addUserRoleEntry", array($session, $t_user,$targetrole));
//			print_r($result);
         	}
	}	
	}
}

function getGlobalRoles ()

{
	
	global $session;
	global $ILIAS_Soap;

	$iliasRoles = $ILIAS_Soap->__soapCall("getRoles", array($session, 'global', -1));
	$iliasRolesXml = new SimpleXmlElement ($iliasRoles);


	foreach ($iliasRolesXml->Role as $role){
        	$role_id = explode('role_',$role->attributes()['id']);
	        $roles_result[(string)$role->Title] = $role_id[1];
	}
	return ($roles_result);

}

function getObjectRoles ($obj_id)

{

	global $session;
	global $ILIAS_Soap;

	$iliasRoles = $ILIAS_Soap->__soapCall("getRoles", array($session, 'local', $obj_id));
//var_dump($iliasRoles);

	$iliasRolesXml = new SimpleXmlElement ($iliasRoles);

//	print_r ($iliasRolesXml);
	foreach ($iliasRolesXml->Role as $role){
		$role_id = explode('role_',$role->attributes()['id']);
        	#print_r($role_id[1]);
	        if ( stripos( $role->Title, 'admin')) {
	                 $roles_result['admin'] = $role_id[1];
	                }
	        if ( stripos( $role->Title, 'tutor')) {
	                 $roles_result['tutor'] = $role_id[1];
	                }
	        if ( stripos( $role->Title, 'member')) {
	                 $roles_result['member'] = $role_id[1];
	                }

	        if ( stripos( $role->Title, 'superior')) {
	                 $roles_result['superior'] = $role_id[1];
	                }
	        if ( stripos( $role->Title, 'employee')) {
	                 $roles_result['employee'] = $role_id[1];
	                }
	        if ( stripos( $role->Title, 'Studienbereiche')) {
			$roles_result['ESC'] = $role_id[1];
			}

	        if ( stripos( $role->Title, 'assist')) {
			$roles_result['testassist'] = $role_id[1];
			}
//		echo "$role->Title\r\n";

	}
	if (isset($roles_result)) {
	  	return ($roles_result);
		} else {
		return;
		}

}

function getRefId($objectname)

{

	global $session;
	global $ILIAS_Soap;
	$iliasObject = $ILIAS_Soap->__soapCall("getObjectsByTitle", array($session, $objectname));
	
	if (empty($iliasObject)) 
		{
		return;
		} else {
		$result = new SimpleXmlElement($iliasObject);
		$objcount = 0;

		foreach ($result->Object as $res){
 			if ($res->Title == $objectname) {
        		$obj_id = $res->References->attributes()['ref_id'];
			$objcount++;

 			}
		}	
                switch ($objcount) {
		   case 0: return;
		   case 1: return($obj_id);
		   default: return (false);

		}
	}
/*
if (isset($obj_id)) {	
			return ($obj_id);
			} else {
			return;
			}
	}
*/
}

function getRolesForName($objectname)

{
	$ref_id = getRefId($objectname);
	if ( $ref_id === false) {
		return (false);
		}
	if (!empty ($ref_id)) {
		return (getObjectRoles($ref_id));
		} else {
		return;
		}
}

function childOus ($refou)

{

	global $session;
	global $ILIAS_Soap;
	global $orgunits;

	static $level = 0;
	$level++;

	$iliasOrgu = $ILIAS_Soap->__soapCall("getTreeChilds", array($session, $refou ,array("orgu")));
	//$iliasOrgu = utf8_encode($ILIAS_Soap->__soapCall("getTreeChilds", array($session, $refou ,array("orgu"))));
	$result_decoded = new SimpleXmlElement( $iliasOrgu );
	$myUnitRef = NULL;
//echo "hier die iliasorgu $iliasOrgu stopp\r\n";
	// Endebedingung der Rekursion
        if (empty ($result_decoded->Object)) {
		$level--;
		return (NULL);
		}

	foreach ($result_decoded->Object as $subou ) {
        	$myUnitName = (string)$subou->Title;
        	$myUnitRef  = (string)$subou->References->attributes()['ref_id'];
        	$orgunits[$myUnitName]['ref'] = $myUnitRef;
        	$orgunits[$myUnitName]['pos'] = 'container';
		
		if ($level > 1) {
			$orgunits[$myUnitName]['affiliation'] = (string)$subou->References->Path->Element[4];
			} else {
			$orgunits[$myUnitName]['affiliation'] = $myUnitName;
			}
			
		$childref = childOus($myUnitRef);

       	  	if (empty ($childref)) $orgunits[$myUnitName]['pos'] = 'leaf';
             
   		
		if ($level == 1) $orgunits[$myUnitName]['pos'] = 'site';

		}
	$level--;
	return ($myUnitRef);
}	

?>
